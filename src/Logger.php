<?php

declare(strict_types=1);

namespace LightSource\Log;

use DateTime;
use Exception;
use LightSource\Log\Interfaces\SettingsInterface;
use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface
{

    /*
     * info : correct works in multiple threads environment :
     * this means lock files && wait threads, this slows down common speed,
     * but it's not doesn't matter much for DEBUG, for LIVE set min level to MAIN,
     * this save from slows down && warranty correct work for multiple threads
     */

    const LEVEL_DEBUG = 'debug';
    const LEVEL_INFO = 'info';
    const LEVEL_NOTICE = 'notice';
    const LEVEL_WARNING = 'warning';
    const LEVEL_ERROR = 'error';
    const LEVEL_CRITICAL = 'critical';
    const LEVEL_ALERT = 'alert';
    const LEVEL_EMERGENCY = 'emergency';

    const WEIGHT_DEBUG = 1;
    const WEIGHT_INFO = 2;
    const WEIGHT_NOTICE = 3;
    const WEIGHT_WARNING = 4;
    const WEIGHT_ERROR = 5;
    const WEIGHT_CRITICAL = 6;
    const WEIGHT_ALERT = 7;
    const WEIGHT_EMERGENCY = 8;

    const WEIGHTS = [
        self::LEVEL_DEBUG     => self::WEIGHT_DEBUG,
        self::LEVEL_INFO      => self::WEIGHT_INFO,
        self::LEVEL_NOTICE    => self::WEIGHT_NOTICE,
        self::LEVEL_WARNING   => self::WEIGHT_WARNING,
        self::LEVEL_ERROR     => self::WEIGHT_ERROR,
        self::LEVEL_CRITICAL  => self::WEIGHT_CRITICAL,
        self::LEVEL_ALERT     => self::WEIGHT_ALERT,
        self::LEVEL_EMERGENCY => self::WEIGHT_EMERGENCY,
    ];

    private SettingsInterface $settings;

    public function __construct(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    private function isAvailableLevel(string $level): bool
    {
        return self::WEIGHTS[$level] >= $this->settings->getMinLevelWeight();
    }

    private function getRandomString(): string
    {
        $symbols = [];

        $symbols = array_merge($symbols, range('a', 'z'));
        $symbols = array_merge($symbols, range('A', 'Z'));
        $symbols = array_merge($symbols, range('0', '9'));

        $lastIndex = count($symbols) - 1;

        $password = '';
        try {
            for ($j = 0; $j < $this->settings->getRandomStringLength(); $j++) {
                $index    = random_int(0, $lastIndex);
                $password .= $symbols[$index];
            }
        } catch (Exception $e) {
            $password = '';
        }

        return $password;
    }

    private function getClassNameWithoutNamespace(string $className): string
    {
        $fromClass = explode('\\', $className);
        $lastIndx  = count($fromClass) - 1;

        return $fromClass[$lastIndx];
    }

    private function getPathToCopyFile(): string
    {
        $dateTime = new DateTime();

        return implode('', [
            $this->settings->getPathToLogDir(),
            DIRECTORY_SEPARATOR . $this->settings->getFileName(),
            '_' . $dateTime->getTimestamp(),
            '_' . $this->getRandomString(),
            '.' . $this->settings->getFileExtension(),
        ]);
    }

    private function getCountLogFiles(): int
    {
        $countLogFiles = 0;

        // array or FALSE
        $fileNames = scandir($this->settings->getPathToLogDir());

        if (! $fileNames) {
            return $countLogFiles;
        }

        $fileNames = array_diff($fileNames, ['.', '..']);

        foreach ($fileNames as $fileName) {
            $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);

            if ($this->settings->getFileExtension() !== $fileExtension) {
                continue;
            }

            $countLogFiles++;
        }

        return $countLogFiles;
    }

    /**
     * Clear log file && clear levels IF file size is will reach limit
     * ( with creating copy before (if have major levels) )
     *
     * @param resource $logFileHandle Must be have cursor at START
     *
     * @return void FileHandle cursor always set at END
     */
    private function rewrite($logFileHandle): void
    {
        // we get file length without filesize function to prevent caching (see filesize function info)
        // set cursor to end
        fseek($logFileHandle, 0, SEEK_END);

        // get file size (it's a current position) - or FALSE, but its equals to 0, so does not need check
        $logFileSize = ftell($logFileHandle);

        // rewrite does not need
        if ($logFileSize < $this->settings->getFileMaxSize()) {
            return;
        }

        $isRequireSave = false;

        // remove all current levels files && check is current log have major levels
        foreach (self::WEIGHTS as $level => $levelWeight) {
            $levelFile = $this->settings->getPathToLevelFile($level);

            if (! is_file($levelFile)) {
                continue;
            }

            unlink($levelFile);

            if (! $isRequireSave &&
                $levelWeight >= $this->settings->getNotRewritableLevel()) {
                $isRequireSave = true;
            }
        }

        // if current log have major levels
        // && count log copies smaller then limit in config
        if ($isRequireSave &&
            $this->getCountLogFiles() < $this->settings->getFileMaxCountCopies()
        ) {
            // put cursor at start
            rewind($logFileHandle);

            // read all file && put cursor at end
            $saveString = fread($logFileHandle, $logFileSize);

            // create copy file
            if ($saveString) {
                $logLevelFileHandle = fopen($this->getPathToCopyFile(), 'c+');
                if ($logLevelFileHandle) {
                    fwrite($logLevelFileHandle, $saveString);
                    fclose($logLevelFileHandle);
                }
            }
        }

        // clear file (cursor does not changed)
        ftruncate($logFileHandle, 0);

        // put cursor to end (because file is cleared && old cursor is wrong)
        fseek($logFileHandle, 0, SEEK_END);

        // clear file cache (in end of all rewrite actions, so only if rewrite is done, not needle if rewrite does not need)
        // (is_file for removed levels can cached, and other file func...)
        clearstatcache();
    }

    // [ [class, type, function ] | [file, line], ]
    private function getBackTrace(bool $isShortClass, int $limit, int $spliceLength): array
    {
        // add splice length to limit

        $limit += $spliceLength;

        $backTraceLines = [];
        $backTrace      = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $limit);

        // Count last not informed call's : delete end (self) functions

        if ($spliceLength &&
            count($backTrace) > $spliceLength) {
            array_splice($backTrace, 0, $spliceLength);
        }

        foreach ($backTrace as $debugInfo) {
            $backTraceLine = [];

            $class = key_exists('class', $debugInfo) ? $debugInfo['class'] : '';
            $class = (! $isShortClass || ! $class) ?
                $class :
                $this->getClassNameWithoutNamespace($class);

            if ($class) {
                $backTraceLine[] = $class;
            }

            if (key_exists('type', $debugInfo)) {
                $backTraceLine[] = $debugInfo['type'];
            }

            if (key_exists('function', $debugInfo)) {
                $backTraceLine[] = $debugInfo['function'];
            }

            if (! $backTraceLine) {
                if (key_exists('file', $debugInfo)) {
                    $backTraceLine[] = $debugInfo['file'];
                }

                if (key_exists('line', $debugInfo)) {
                    $backTraceLine[] = $debugInfo['line'];
                }
            }

            $backTraceLines[] = $backTraceLine;
        }

        return $backTraceLines;
    }

    private function prepareLog(
        string $level,
        string $message,
        array $info
    ): string {
        $logLines = [];

        $backTraceInfo = $this->getBackTrace(
            $this->settings->isShortLogClass(),
            $this->settings->getBackTraceLimit(),
            $this->settings->getBackTraceSplice()
        );

        // if detected : class name OR filename

        $endSource = ($backTraceInfo &&
                      $backTraceInfo[0]) ?
            $backTraceInfo[0][0] :
            '';

        $logLines[] = implode(' : ', [strtoupper($level), $endSource, $message,]);

        // convert backTraceInfo sub-arrays to strings

        for ($i = 0; $i < count($backTraceInfo); $i++) {
            $backTraceInfo[$i] = implode('', $backTraceInfo[$i]);
        }

        // implode

        $logLines[] = implode(' ; ', $backTraceInfo);

        if ($info) {
            $printRString = print_r($info, true);
            $printRString = rtrim($printRString, "\n");
            $logLines[]   = "info : <!-- {$printRString} -->";
        }

        $dateTime   = new DateTime();
        $logLines[] = $dateTime->format('Y-m-d H:i:s');

        return implode("\n", $logLines) . "\n\n";
    }

    public function getSettings(): Settings
    {
        return $this->settings;
    }

    ////

    public function emergency($message, array $context = []): void
    {
        $this->log(self::LEVEL_EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = []): void
    {
        $this->log(self::LEVEL_ALERT, $message, $context);
    }

    public function critical($message, array $context = []): void
    {
        $this->log(self::LEVEL_CRITICAL, $message, $context);
    }

    public function error($message, array $context = []): void
    {
        $this->log(self::LEVEL_ERROR, $message, $context);
    }

    public function warning($message, array $context = []): void
    {
        $this->log(self::LEVEL_WARNING, $message, $context);
    }

    public function notice($message, array $context = []): void
    {
        $this->log(self::LEVEL_NOTICE, $message, $context);
    }

    public function info($message, array $context = []): void
    {
        $this->log(self::LEVEL_INFO, $message, $context);
    }

    public function debug($message, array $context = []): void
    {
        $this->log(self::LEVEL_DEBUG, $message, $context);
    }

    /*
     * do not use this method directly without require
     * (because Settings->backTraceSplice has setup for a chain, like info()->log() and otherwise it'll
     * mark the Log class as the last, i.e. 'INFO : Log : {message}')
     */
    public function log($level, $message, array $context = []): void
    {
        if (! key_exists($level, self::WEIGHTS) ||
            ! $this->isAvailableLevel($level)) {
            return;
        }

        $logString = $this->prepareLog(
            $level,
            $message,
            $context
        );

        // open for read/write (created if not exist), not reduced, put cursor at START
        $pathToFile    = $this->settings->getPathToLogFile();
        $logFileHandle = fopen($pathToFile, 'c+');

        // something wrong
        if (false === $logFileHandle) {
            return;
        }

        // try lock file with exclusive rights, thread is stopped until receipt
        if (! flock($logFileHandle, LOCK_EX)) {
            fclose($logFileHandle);

            return;
        }

        // checks for rewrite AND always set cursor at END
        $this->rewrite($logFileHandle);
        // write log line
        fwrite($logFileHandle, $logString);
        // clear output before unlock
        fflush($logFileHandle);
        // unlock
        flock($logFileHandle, LOCK_UN);
        // close file
        fclose($logFileHandle);

        // create level file (if not exists)
        $levelFile = $this->settings->getPathToLevelFile($level);
        if (! is_file($levelFile)) {
            $logLevelFileHandle = fopen($levelFile, 'c+');
            if ($logLevelFileHandle) {
                fclose($logLevelFileHandle);
            }
        }

        // notify
        if (self::WEIGHTS[$level] >= $this->settings->getNotificationMinLevel() && is_callable(
                $this->settings->getNotificationCallback()
            )) {
            call_user_func_array($this->settings->getNotificationCallback(), [
                'level' => $level,
            ]);
        }
    }
}
