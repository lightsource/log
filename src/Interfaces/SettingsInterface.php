<?php

declare(strict_types=1);

namespace LightSource\Log\Interfaces;

interface SettingsInterface{
    ////

    public function getPathToLevelFile(string $level): string;

    public function getPathToLogFile(): string;

    ////

    public function setPathToLogDir(string $pathToLogDir): void;

    public function setFileName(string $fileName): void;

    public function setFileExtension(string $fileExtension): void;

    public function setLevelFileExtension(string $levelFileExtension): void;

    public function setFileMaxSize(int $fileMaxSize): void;

    public function setFileMaxCountCopies(int $fileMaxCountCopies): void;

    public function setMinLevelWeight(int $minLevelWeight): void;

    public function setIsShortLogClass(bool $isShortLogClass): void;

    public function setNotRewritableLevel(int $notRewritableLevel): void;

    public function setNotificationCallback(?callable $notificationCallback): void;

    public function setNotificationMinLevel(int $notificationMinLevel): void;

    public function setBackTraceLimit(int $backTraceLimit): void;

    public function setBackTraceSplice(int $backTraceSplice): void;

    public function setRandomStringLength(int $randomStringLength): void;

    public function getPathToLogDir(): string;

    public function getFileName(): string;

    public function getFileExtension(): string;

    public function getLevelFileExtension(): string;

    public function getFileMaxSize(): int;

    public function getFileMaxCountCopies(): int;

    public function getMinLevelWeight(): int;

    public function isShortLogClass(): bool;

    public function getNotRewritableLevel(): int;

    public function getNotificationCallback(): ?callable;

    public function getNotificationMinLevel(): int;

    public function getBackTraceLimit(): int;

    public function getBackTraceSplice(): int;

    public function getRandomStringLength(): int;
}
