<?php

declare(strict_types=1);

namespace LightSource\Log;

use LightSource\Log\Interfaces\SettingsInterface;

class Settings implements SettingsInterface
{
    private string $pathToLogDir;
    private string $fileName;
    private string $fileExtension;
    private string $levelFileExtension;
    private int $fileMaxSize;
    private int $fileMaxCountCopies;
    private int $minLevelWeight;
    private bool $isShortLogClass;
    private int $notRewritableLevel;
    /**
     * @var callable|null
     */
    private $notificationCallback;
    private int $notificationMinLevel;
    private int $backTraceLimit;
    private int $backTraceSplice; // _BackTrace() + _PrepareLog() + Write()
    private int $randomStringLength;

    public function __construct()
    {
        $this->pathToLogDir         = '';
        $this->fileName             = 'log';
        $this->fileExtension        = 'html';
        $this->levelFileExtension   = 'level';
        $this->fileMaxSize          = 409600;
        $this->fileMaxCountCopies   = 5;
        $this->minLevelWeight       = Logger::WEIGHT_DEBUG;
        $this->isShortLogClass      = true;
        $this->notRewritableLevel   = Logger::WEIGHT_WARNING;
        $this->notificationCallback = null;
        $this->notificationMinLevel = Logger::WEIGHT_WARNING;
        $this->backTraceLimit       = 3;
        //  exclude self non-informative methods - debug(); log(); prepareLog(); getBackTrace();
        $this->backTraceSplice    = 4;
        $this->randomStringLength = 8;
    }

    ////

    public function getPathToLevelFile(string $level): string
    {
        return $this->pathToLogDir . DIRECTORY_SEPARATOR . $level . '.' . $this->levelFileExtension;
    }

    public function getPathToLogFile(): string
    {
        return $this->pathToLogDir . DIRECTORY_SEPARATOR . $this->fileName . '.' . $this->fileExtension;
    }

    ////

    public function setPathToLogDir(string $pathToLogDir): void
    {
        $this->pathToLogDir = $pathToLogDir;
    }

    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    public function setFileExtension(string $fileExtension): void
    {
        $this->fileExtension = $fileExtension;
    }

    public function setLevelFileExtension(string $levelFileExtension): void
    {
        $this->levelFileExtension = $levelFileExtension;
    }

    public function setFileMaxSize(int $fileMaxSize): void
    {
        $this->fileMaxSize = $fileMaxSize;
    }

    public function setFileMaxCountCopies(int $fileMaxCountCopies): void
    {
        $this->fileMaxCountCopies = $fileMaxCountCopies;
    }

    public function setMinLevelWeight(int $minLevelWeight): void
    {
        $this->minLevelWeight = $minLevelWeight;
    }

    public function setIsShortLogClass(bool $isShortLogClass): void
    {
        $this->isShortLogClass = $isShortLogClass;
    }

    public function setNotRewritableLevel(int $notRewritableLevel): void
    {
        $this->notRewritableLevel = $notRewritableLevel;
    }

    public function setNotificationCallback(?callable $notificationCallback): void
    {
        $this->notificationCallback = $notificationCallback;
    }

    public function setNotificationMinLevel(int $notificationMinLevel): void
    {
        $this->notificationMinLevel = $notificationMinLevel;
    }

    public function setBackTraceLimit(int $backTraceLimit): void
    {
        $this->backTraceLimit = $backTraceLimit;
    }

    public function setBackTraceSplice(int $backTraceSplice): void
    {
        $this->backTraceSplice = $backTraceSplice;
    }

    public function setRandomStringLength(int $randomStringLength): void
    {
        $this->randomStringLength = $randomStringLength;
    }

    public function getPathToLogDir(): string
    {
        return $this->pathToLogDir;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    public function getLevelFileExtension(): string
    {
        return $this->levelFileExtension;
    }

    public function getFileMaxSize(): int
    {
        return $this->fileMaxSize;
    }

    public function getFileMaxCountCopies(): int
    {
        return $this->fileMaxCountCopies;
    }

    public function getMinLevelWeight(): int
    {
        return $this->minLevelWeight;
    }

    public function isShortLogClass(): bool
    {
        return $this->isShortLogClass;
    }

    public function getNotRewritableLevel(): int
    {
        return $this->notRewritableLevel;
    }

    public function getNotificationCallback(): ?callable
    {
        return $this->notificationCallback;
    }

    public function getNotificationMinLevel(): int
    {
        return $this->notificationMinLevel;
    }

    public function getBackTraceLimit(): int
    {
        return $this->backTraceLimit;
    }

    public function getBackTraceSplice(): int
    {
        return $this->backTraceSplice;
    }

    public function getRandomStringLength(): int
    {
        return $this->randomStringLength;
    }
}