<?php

declare(strict_types=1);

namespace LightSource\Log\Tests\unit;

use Codeception\Test\Unit;
use DateTime;
use LightSource\Log\Logger;
use LightSource\Log\Settings;
use org\bovigo\vfs\vfsStream;

class LogTest extends Unit
{

    // get a unique directory name depending on a test method to prevent affect other tests
    private static function getUniqueDirName(string $methodConstant): string
    {
        return str_replace([':', '\\'], '_', $methodConstant);
    }

    private static function getLog(string $methodConstant): Logger
    {
        $directory = vfsStream::setup(self::getUniqueDirName($methodConstant));
        vfsStream::create([], $directory);

        $settings = new Settings();
        $settings->setPathToLogDir($directory->url());

        return new Logger($settings);
    }

    private static function getFolderList(string $folder): array
    {
        $listOfFiles = scandir($folder);
        $listOfFiles = array_diff($listOfFiles, ['.', '..',]);

        return array_values($listOfFiles);
    }

    private static function getShortMethodName(string $methodConstant): string
    {
        $methodName = explode('::', $methodConstant);

        return $methodName[count($methodName) - 1];
    }

    public function testWriteFiles()
    {
        $log = self::getLog(__METHOD__);

        $log->info('test');

        $listOfFiles = self::getFolderList($log->getSettings()->getPathToLogDir());
        $this->assertEquals(['info.level', 'log.html'], $listOfFiles);
    }

    public function testIgnoreWriteFilesWhenIgnoredLevel()
    {
        $log = self::getLog(__METHOD__);
        $log->getSettings()->setMinLevelWeight(Logger::WEIGHT_INFO);

        $log->debug('test');

        $listOfFiles = self::getFolderList($log->getSettings()->getPathToLogDir());
        $this->assertEmpty($listOfFiles);
    }

    public function testFormat()
    {
        $log        = self::getLog(__METHOD__);
        $methodName = self::getShortMethodName(__METHOD__);

        $dateTime = new DateTime();
        $log->info('test');

        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : LogTest : test" .
                              "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                              $logDateTime .
                              "\n\n";
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testFormatWithoutShortClass()
    {
        $log = self::getLog(__METHOD__);
        $log->getSettings()->setIsShortLogClass(false);
        $class      = self::class;
        $methodName = str_replace('::', '->', __METHOD__);

        $dateTime = new DateTime();
        $log->info('test');

        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : {$class} : test" .
                              "\n{$methodName} ; PHPUnit\Framework\TestCase->runTest ; PHPUnit\Framework\TestCase->runBare\n" .
                              $logDateTime .
                              "\n\n";
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testFormatWithAnotherBackTraceLimit()
    {
        $log = self::getLog(__METHOD__);
        $log->getSettings()->setBackTraceLimit(5);
        $methodName = self::getShortMethodName(__METHOD__);

        $dateTime = new DateTime();
        $log->info('test');

        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : LogTest : test" .
                              "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare ; TestResult->run ; TestCase->run\n" .
                              $logDateTime .
                              "\n\n";
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testFormatWithBackTraceSplice()
    {
        $log = self::getLog(__METHOD__);
        $log->getSettings()->setBackTraceSplice(5);
        $dateTime = new DateTime();

        $log->info('test');

        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : TestCase : test" .
                              "\nTestCase->runTest ; TestCase->runBare ; TestResult->run\n" .
                              $logDateTime .
                              "\n\n";
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testNotificationCallback()
    {
        $isCalled = false;
        $log      = self::getLog(__METHOD__);
        $log->getSettings()->setNotificationCallback(function (string $level) use (&$isCalled) {
            $isCalled = true;
        });
        $log->getSettings()->setNotificationMinLevel(Logger::WEIGHT_WARNING);

        $log->warning('test');

        $this->assertTrue($isCalled);
    }

    public function testIgnoreNotificationCallbackWhenIgnoredLevel()
    {
        $isCalled = false;
        $log      = self::getLog(__METHOD__);
        $log->getSettings()->setNotificationCallback(function (string $level) use (&$isCalled) {
            $isCalled = true;
        });
        $log->getSettings()->setNotificationMinLevel(Logger::WEIGHT_WARNING);

        $log->notice('test');

        $this->assertFalse($isCalled);
    }

    public function testIgnoreRewriteWhenSizeIsSmallerLimit()
    {
        $log        = self::getLog(__METHOD__);
        $methodName = self::getShortMethodName(__METHOD__);

        $log->info('first');
        $logSize = filesize($log->getSettings()->getPathToLogFile());
        $log->getSettings()->setFileMaxSize($logSize + 1);
        $dateTime = new DateTime();
        $log->info('second');

        $listOfFiles        = self::getFolderList($log->getSettings()->getPathToLogDir());
        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : LogTest : first" .
                              "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                              $logDateTime .
                              "\n\n";
        $expectedLogContent .= "INFO : LogTest : second" .
                               "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                               $logDateTime .
                               "\n\n";
        $this->assertEquals(['info.level', 'log.html'], $listOfFiles);
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testRewriteWithoutBackup()
    {
        $log        = self::getLog(__METHOD__);
        $methodName = self::getShortMethodName(__METHOD__);

        $log->debug('first');
        $logSize = filesize($log->getSettings()->getPathToLogFile());
        $log->getSettings()->setFileMaxSize($logSize);
        $dateTime = new DateTime();
        $log->info('second');

        $listOfFiles        = self::getFolderList($log->getSettings()->getPathToLogDir());
        $logContent         = file_get_contents($log->getSettings()->getPathToLogFile());
        $logDateTime        = $dateTime->format('Y-m-d H:i:s');
        $expectedLogContent = "INFO : LogTest : second" .
                              "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                              $logDateTime .
                              "\n\n";
        $this->assertEquals(['info.level', 'log.html'], $listOfFiles);
        $this->assertEquals($expectedLogContent, $logContent);
    }

    public function testRewriteWithBackup()
    {
        $log        = self::getLog(__METHOD__);
        $methodName = self::getShortMethodName(__METHOD__);

        $log->critical('first');
        $dateTimeBackup = new DateTime();
        $logSize        = filesize($log->getSettings()->getPathToLogFile());
        $log->getSettings()->setFileMaxSize($logSize);
        $dateTime = new DateTime();
        $log->info('second');

        $listOfFiles           = self::getFolderList($log->getSettings()->getPathToLogDir());
        $backupFile            = array_filter($listOfFiles, function ($file) {
            return 0 === strpos($file, 'log_');
        });
        $backupFile            = array_values($backupFile);
        $backupFile            = isset($backupFile[0]) ?
            $log->getSettings()->getPathToLogDir() . '/' . $backupFile[0] :
            '';
        $backupFileName        = explode('/', $backupFile);
        $backupFileName        = $backupFileName[count($backupFileName) - 1];
        $logContent            = file_get_contents($log->getSettings()->getPathToLogFile());
        $backupContent         = file_get_contents($backupFile);
        $logDateTime           = $dateTime->format('Y-m-d H:i:s');
        $backupDateTime        = $dateTimeBackup->format('Y-m-d H:i:s');
        $expectedLogContent    = "INFO : LogTest : second" .
                                 "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                                 $logDateTime .
                                 "\n\n";
        $expectedBackupContent = "CRITICAL : LogTest : first" .
                                 "\nLogTest->{$methodName} ; TestCase->runTest ; TestCase->runBare\n" .
                                 $backupDateTime .
                                 "\n\n";
        $this->assertEquals(['info.level', 'log.html', $backupFileName], $listOfFiles);
        $this->assertEquals($expectedLogContent, $logContent);
        $this->assertEquals($expectedBackupContent, $backupContent);
    }

    public function testIgnoreRewriteWhenMaxBackupCountIsReached()
    {
        $log            = self::getLog(__METHOD__);
        $maxCountCopies = $log->getSettings()->getFileMaxCountCopies();
        $countCopies    = $maxCountCopies + 1;
        for ($i = 1; $i <= $countCopies; $i++) {
            $log->critical('critical');
            $logSize = filesize($log->getSettings()->getPathToLogFile());
            $log->getSettings()->setFileMaxSize($logSize);
        }

        $listOfFiles = self::getFolderList($log->getSettings()->getPathToLogDir());
        $this->assertCount(6, $listOfFiles);
    }
}
