# Log

## What is it
PSR-3 compatible logging with multiple levels and support multi-thread working

## Installation
```
composer require lightsource/log
```

## Example of usage

```
require_once __DIR__ . '/vendor/autoload.php';

$settings = new \LightSource\Log\Settings();
$settings->setPathToLogDir(__DIR__ . DIRECTORY_SEPARATOR . 'Logs');
$logger = new \LightSource\Log\Logger($settings);

$logger->debug('New log line', [
  'someData' => 'test',
]);
```
## Example of output
```
DEBUG : ClassName : New log line
ClassName->methodName ; PreviousClass->method2 ; PreviousClass->method1
info : <!-- Array
(
   [someData] => test
) -->
2021-10-04 18:17:40
```
